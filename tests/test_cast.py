import pytest

from tipsy import cast_to_type

from .types import YamlValues


def test_basic():
    var: YamlValues = "asdf"
    res = cast_to_type(var, str)
    assert type(res) is str
    with pytest.raises(TypeError):
        cast_to_type(var, list)
    with pytest.raises(TypeError):
        cast_to_type(var, dict)


def test_basic2():
    var: YamlValues = [1, 2, 3, "asdf"]
    res = cast_to_type(var, list)
    assert type(res) is list
    with pytest.raises(TypeError):
        cast_to_type(var, str)
    with pytest.raises(TypeError):
        cast_to_type(var, dict)


def test_basic3():
    var: YamlValues = {"a": "asdf"}
    res = cast_to_type(var, dict)
    assert type(res) is dict
    with pytest.raises(TypeError):
        cast_to_type(var, str)
    with pytest.raises(TypeError):
        cast_to_type(var, list)
