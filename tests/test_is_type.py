from collections.abc import Callable, Mapping, Sequence
from types import NoneType
from typing import Any

from dacite import from_dict

from tipsy import is_type

from .types import Lit, MyDataClass, MyDict, MyDictOptional, MyInt, MyOtherDataClass, MySecondOrderDict


def test_basic():
    assert is_type("asdf", str) is True
    assert is_type(5, str) is False


def test_options():
    assert is_type(["a", "b", "c"], list[str | int]) is True
    assert is_type(["a", 5, "c"], list[str | int]) is True
    assert is_type(["a", 5.0, "c"], list[str | int]) is False


def test_literal():
    assert is_type("a", Lit) is True  # type: ignore[arg-type]
    assert is_type("b", Lit) is True  # type: ignore[arg-type]
    assert is_type("e", Lit) is False  # type: ignore[arg-type]
    assert is_type(5.0, Lit) is False  # type: ignore[arg-type]


def test_generic_list():
    assert is_type(["a", "b", "c"], list[str]) is True
    assert is_type(["a", 5, "c"], list[str]) is False


def test_generic_set():
    assert is_type({"a", "b", "c"}, set[str]) is True
    assert is_type({"a", 5, "c"}, set[str]) is False


def test_generic_dict():
    assert is_type({"a": "asdf", "q": "qwer"}, dict[str, str]) is True
    assert is_type({"a": "asdf", "q": 5}, dict[str, str]) is False
    assert is_type({"a": "asdf", 5: "qwer"}, dict[str, str]) is False


def test_generic_tuple():
    assert is_type(("asdf", 5), tuple[str, int]) is True
    assert is_type((5, "asdf"), tuple[str, int]) is False
    assert is_type(("asdf", "qwer"), tuple[str, int]) is False


def test_none():
    assert is_type(None, NoneType) is True
    assert is_type(5, NoneType) is False


def test_callable():
    assert is_type(lambda x: None, Callable) is True  # type: ignore[arg-type]
    assert is_type(5, Callable) is False  # type: ignore[arg-type]


def test_generic_type():
    # mypy does not yet handle 'type[]' correctly
    # https://github.com/python/mypy/issues/12392
    assert is_type(int, type[int]) is True  # type: ignore[index]
    assert is_type(5, type[int]) is False  # type: ignore[index]
    assert is_type(str, type[int]) is False  # type: ignore[index]


def test_mutable_collections_are_invariant_in_strict_mode():
    assert is_type(["a", "b", "c"], list[Any], strict=True) is False
    assert is_type({"a": "asdf", "b": "qwer"}, dict[str, Any], strict=True) is False


def test_mutable_collections_are_invariant_in_strict_mode_over_union():
    assert is_type(["a", "b", "c"], list[str | int], strict=True) is False
    assert is_type({"a": "asdf", "b": "qwer"}, dict[str, str | int], strict=True) is False


def test_mutable_collections_invariance_is_ignored_by_default():
    assert is_type(["a", "b", "c"], list[Any]) is True
    assert is_type({"a": "asdf", "b": "qwer"}, dict[str, Any]) is True


def test_immutable_collections_are_covariant():
    # mypy disallows passing abstract types to functions
    # https://github.com/python/mypy/issues/4717
    assert is_type(["a", "b", "c"], Sequence[Any]) is True  # type: ignore[type-abstract]
    assert is_type({"a": "asdf", "b": "qwer"}, Mapping[str, Any]) is True  # type: ignore[type-abstract]


def test_immutable_collections_are_covariant_over_union():
    assert is_type(["a", "b", "c"], Sequence[str | int]) is True  # type: ignore[type-abstract]
    assert is_type({"a": "asdf", "b": "qwer"}, Mapping[str, str | int]) is True  # type: ignore[type-abstract]


def test_typed_dict():
    assert is_type({"a": "asdf", "b": "qwer", "c": ["asdf"]}, MyDict) is True
    assert is_type({"a": "asdf", "b": "qwer"}, MyDict) is False
    assert is_type({"a": "asdf", "b": "qwer", "c": [1]}, MyDict) is False


def test_typed_dict_with_optional_args():
    assert is_type({"a": "asdf", "b": "qwer", "c": ["asdf"]}, MyDictOptional) is True
    assert is_type({"a": "asdf", "b": "qwer"}, MyDictOptional) is True
    assert is_type({"a": "asdf", "b": "qwer", "c": [1]}, MyDictOptional) is False


def test_list_of_typed_dict():
    assert is_type([{"a": "asdf", "b": "qwer", "c": ["asdf"]}], list[MyDict]) is True
    assert is_type([{"a": "asdf", "b": "qwer"}], list[MyDict]) is False
    assert is_type([{"a": "asdf", "b": "qwer", "c": [1]}], list[MyDict]) is False


def test_second_order_typed_dict():
    assert is_type({"a": [{"a": "asdf", "b": "qwer", "c": ["asdf"]}]}, MySecondOrderDict) is True
    assert is_type({"a": [{"a": "asdf", "b": "qwer"}]}, MySecondOrderDict) is False
    assert is_type({"a": [{"a": "asdf", "b": "qwer", "c": [1]}]}, MySecondOrderDict) is False


def test_data_class():
    assert is_type(from_dict(MyDataClass, {"a": "asdf", "b": "qwer", "c": ["asdf"]}), MyDataClass) is True
    assert is_type(from_dict(MyOtherDataClass, {"a": "asdf", "b": "qwer", "c": ["asdf"]}), MyDataClass) is False


def test_new_type_defaults_to_parent():
    assert is_type(5, MyInt) is True
    assert is_type("asdf", MyInt) is False


def test_new_type_does_not_default_to_parent_in_strict_mode():
    assert is_type(5, MyInt, strict=True) is False


def test_generic_callable():
    def fnc(x: int) -> str:
        return str(x)

    assert is_type(fnc, Callable[[int], str]) is True  # type: ignore[arg-type]
    assert is_type(fnc, Callable[[int, int], str]) is False  # type: ignore[arg-type]
    assert is_type(fnc, Callable[[int], int]) is False  # type: ignore[arg-type]
    assert is_type(fnc, Callable[[str], str]) is False  # type: ignore[arg-type]
