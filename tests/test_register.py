import os
import re
from typing import NewType, TypeGuard

from tipsy import is_type, nongeneric, register_type, subtype

Path = NewType("Path", str)
RelPath = NewType("RelPath", Path)
AbsPath = NewType("AbsPath", Path)


@register_type(Path)
@subtype(str)
@nongeneric
def _(data: str) -> TypeGuard[Path]:
    return bool(re.match(r"^(\.{0,2})\/([^\/]+)$", data))


@register_type(AbsPath)
@subtype(Path)
@nongeneric
def _(data: Path) -> TypeGuard[AbsPath]:
    return os.path.isabs(data)


@register_type(RelPath)
@subtype(Path)
@nongeneric
def _(data: Path) -> TypeGuard[RelPath]:
    return not os.path.isabs(data)


def test_is_relpath():
    assert is_type("./asdf", RelPath) is True
    assert is_type("/asdf", RelPath) is False


def test_collection():
    assert is_type(["/asdf", "/qwer"], list[AbsPath]) is True
    assert is_type(["/asdf", "./qwer"], list[AbsPath]) is False
