from dataclasses import dataclass
from typing import Literal, NewType, TypeAlias, TypedDict


class MyDict(TypedDict):
    a: str
    b: str
    c: list[str]


class MyDictOptional(TypedDict, total=False):
    a: str
    b: str
    c: list[str]


class MySecondOrderDict(TypedDict):
    a: list[MyDict]


@dataclass
class MyDataClass:
    a: str
    b: str
    c: list[str]


@dataclass
class MyOtherDataClass:
    a: str
    b: str
    c: list[str]


MyInt = NewType("MyInt", int)

YamlValues: TypeAlias = dict | list | str

Lit = Literal["a", "b", "c", "d"]
